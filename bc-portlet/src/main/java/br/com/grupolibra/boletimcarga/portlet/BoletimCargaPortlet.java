package br.com.grupolibra.boletimcarga.portlet;

import java.io.IOException;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowState;

/**
 * Controle do portlet
 * @author joel.backschat
 * @since 07/08/2014
 * @version 1.0
 */
public class BoletimCargaPortlet extends GenericPortlet {

    private static final String NORMAL_VIEW = "/index.jsp";
    private static final String MAXIMIZED_VIEW = "/index.jsp";
    private static final String HELP_VIEW = "/help.jsp";
    private PortletRequestDispatcher normalView;
    private PortletRequestDispatcher maximizedView;
    private PortletRequestDispatcher helpView;

    public void doView( RenderRequest request, RenderResponse response )
        throws PortletException, IOException {
    	
    	 normalView = request.getPortletSession().getPortletContext().getRequestDispatcher( NORMAL_VIEW );
         maximizedView = request.getPortletSession().getPortletContext().getRequestDispatcher( MAXIMIZED_VIEW );
         helpView = request.getPortletSession().getPortletContext().getRequestDispatcher( HELP_VIEW );
         
         if( WindowState.MINIMIZED.equals( request.getWindowState() ) ) {
             return;
         }
  
         if ( WindowState.NORMAL.equals( request.getWindowState() ) ) {
             normalView.include( request, response );
         } else {
             maximizedView.include( request, response );
         }
    }

    protected void doHelp( RenderRequest request, RenderResponse response )
        throws PortletException, IOException {
        helpView.include( request, response );
    }

    @Override
    public void init( PortletConfig config ) throws PortletException {
        super.init( config );
    }
    
    @Override
    public void init() throws PortletException {
    	super.init();
    }

    public void destroy() {
        normalView = null;
        maximizedView = null;
        helpView = null;
        super.destroy();
    }
}