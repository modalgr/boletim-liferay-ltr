BoletimApp.controller('BoletimCargaController', ['BoletimCargaService', '$interval', function(BoletimCargaService) {

    var vm = this;

    vm.showDialog = false;
    vm.showLoad = false;
    vm.openRow;
    vm.promise;
    vm.hasEcpf = "Não detectado!";
    vm.fields;
    vm.showCombo = false;
    vm.options;
    vm.vesselPesq = "";
    vm.ecpfNbr = "0";
    vm.searchParams;
    vm.VesselVisitFlag = false;
    vm.GKeyFlag = false;
    vm.DataFlag = false;
    vm.StatusFlag = false;
    vm.fieldOptions = "";

    vm.vessels = [];
    vm.boletins = [{ boletimId: 0, dataRetornoFormatada: '04/12/2017', operacao: 'AA', restow: 'AA', status: 'Cancelado' }];
    vm.boletinsPendentes = [];
    vm.SelectedSearchOption = {};
    vm.SearchOptions = [{ ID: 0, Description: 'Vessel Visit', value: '' }, { ID: 1, Description: 'GKey', value: '' }, { ID: 2, Description: 'Data', value: '' }, { ID: 3, Description: 'Status', value: '' }, { ID: 4, Description: 'Selecionar Todos', value: '' }]

    vm.ShowField = ShowField;
    vm.toUpperCase = toUpperCase;
    vm.pesquisarVesselVisit = pesquisarVesselVisit;
    vm.pesquisarSearchFields = pesquisarSearchFields;
    vm.reSend = reSend;
    vm.changeCombo = changeCombo;
    vm.limparInfo = limparInfo;
    vm.addChildRow = addChildRow;
    vm.formatChildResultRow = formatChildResultRow;
    vm.to_trusted = to_trusted;
    vm.closeDialog = closeDialog;
    vm.closeDialogSuccess = closeDialogSuccess;
    vm.formatChildRow = formatChildRow;
    vm.addLogResult = addLogResult;
    vm.applyEvents = applyEvents;
    vm.detectarEcpf = detectarEcpf;
    vm.startInterval = startInterval;

    function ShowField() {
        vm.fieldOptions = "";
        switch (vm.SelectedSearchOption.ID) {
            case 0:
                vm.VesselVisitFlag = true;
                vm.GKeyFlag = false;
                vm.DataFlag = false;
                vm.StatusFlag = false;
                break;
            case 1:
                vm.VesselVisitFlag = false;
                vm.GKeyFlag = true;
                vm.DataFlag = false;
                vm.StatusFlag = false;
                break;
            case 2:
                vm.VesselVisitFlag = false;
                vm.GKeyFlag = false;
                vm.DataFlag = true;
                vm.StatusFlag = false;
                break;
            case 3:
                vm.VesselVisitFlag = false;
                vm.GKeyFlag = false;
                vm.DataFlag = false;
                vm.StatusFlag = true;
                break;
            case 4:
                vm.VesselVisitFlag = false;
                vm.GKeyFlag = false;
                vm.DataFlag = false;
                vm.StatusFlag = false;
                break;
        }
    }

    function toUpperCase() {
        vm.fieldCont = vm.fieldCont.toUpperCase();
    };

    function pesquisarVesselVisit() {
        $('#showLoadModal').show();
        BoletimCargaService.pesquisarVesselVisit(null, function(success) {
            $('#showLoadModal').hide();
            vm.vessels = success;
        }, function(error) {
            $('#showLoadModal').hide();
            $("body").css("cursor", "");
            vm.showDialog = true;
            vm.errorMessage = error.data.replace(/(\"|\\n)/gi, "<br>");
        });
    };

    function pesquisarSearchFields() {
        $('#showLoadModal').show();
        vm.SelectedSearchOption.value = vm.fieldOptions;
        BoletimCargaService.pesquisarSearchFields(vm.SelectedSearchOption).then(function(success) {
            $('#showLoadModal').hide();
            vm.fields = success;
            for (var int = 0; int < vm.fields.length; int++) {
                vm.fields[int].desc = decodeURIComponent(escape(vm.fields[int].desc));
            }
        }, function(error) {
            $('#showLoadModal').hide();
            $("body").css("cursor", "");
            vm.showDialog = true;
            vm.errorMessage = error.data.replace(/(\"|\\n)/gi, "<br>");
        });
    };

    vm.reSend = function(unitNbr) {
        $('#showLoadModal').show();

        if (unitNbr.status == " ") {
            BoletimCargaService
                .generateXML(
                    unitNbr,
                    function(success) {
                        if ($('#boletins') !== undefined && $('#boletins').isDataTable()) {
                            $('#boletins').DataTable().destroy();
                        }

                        if ($('#boletinsPendentes') !== undefined &&
                            $('#boletinsPendentes').isDataTable()) {
                            $('#boletinsPendentes').DataTable().destroy();
                        }

                        if ($('#boletimDetail') !== undefined &&
                            $('#boletimDetail').isDataTable()) {
                            $('#boletimDetail').DataTable().destroy();
                        }

                        if ($('#boletimDetailPendentes') !== undefined &&
                            $('#boletimDetailPendentes').isDataTable()) {
                            $('#boletimDetailPendentes').DataTable().destroy();
                        }

                        $('#showLoadModal').hide();

                        BoletimCargaService
                            .findVesselVisitInfo(
                                vm.searchParams,
                                function(success) {
                                    vm.vesselPesq = vm.vessel;

                                    $('#showLoadModal').hide();
                                    vm.boletins = success;
                                })
                    },
                    function(error) {
                        $('#showLoadModal').hide();
                        $("body").css("cursor", "");
                        vm.showDialog = true;
                        vm.errorMessage = error.data.replace(
                            /(\"|\\n)/gi, "<br>");
                    });
        } else {
            var params = {
                unitNbr: unitNbr,
                vessel: vm.vesselPesq.gkey
            };

            BoletimCargaService
                .reGenerateXML(
                    params,
                    function(success) {
                        if ($('#boletins') !== undefined && $('#boletins').isDataTable()) {
                            $('#boletins').DataTable().destroy();
                        }

                        if ($('#boletinsPendentes') !== undefined &&
                            $('#boletinsPendentes').isDataTable()) {
                            $('#boletinsPendentes').DataTable().destroy();
                        }

                        if ($('#boletimDetail') !== undefined &&
                            $('#boletimDetail').isDataTable()) {
                            $('#boletimDetail').DataTable().destroy();
                        }

                        if ($('#boletimDetailPendentes') !== undefined &&
                            $('#boletimDetailPendentes').isDataTable()) {
                            $('#boletimDetailPendentes').DataTable().destroy();
                        }

                        $('#showLoadModal').hide();

                        BoletimCargaService
                            .findVesselVisitInfo(
                                vm.searchParams,
                                function(success) {
                                    vm.vesselPesq = vm.vessel;

                                    $('#showLoadModal').hide();
                                    vm.boletins = success;
                                })
                    },
                    function(error) {
                        $('#showLoadModal').hide();
                        $("body").css("cursor", "");
                        vm.showDialog = true;
                        vm.errorMessage = error.data.replace(
                            /(\"|\\n)/gi, "<br>");
                    });
        }
    };


    function changeCombo(field) {
        vm.showCombo = field.comboBox;

        vm.options = [];

        for (var i = 0; i < field.comboValues.length; i++) {
            var option = {
                id: i,
                descricao: field.comboValues[i]
            };
            vm.options[i] = option;
        }

        vm.fieldCont = "";
    };

    function limparInfo() {
        vm.fieldCont = "";
        vm.vessel = "";
        vm.vesselPesq = "";
        vm.boletins = undefined;
        vm.searchParams = undefined;
    }

    function findVesselVisitInfo() {
        vm.boletins = undefined;

        if (vm.vessel == undefined || vm.vessel == "") {
            $("body").css("cursor", "");
            vm.showDialog = true;
            vm.errorMessage = "É necessário selecionar uma Vessel Visit.";
            return;
        }

        if (vm.fieldCont != undefined &&
            vm.fieldCont.descricao != undefined) {
            vm.searchParams = {
                vessel: vm.vessel,
                field: vm.field,
                value: vm.fieldCont.descricao
            };
        } else {
            vm.searchParams = {
                vessel: vm.vessel,
                field: vm.field,
                value: vm.fieldCont
            };
        }

        if ($('#boletins') !== undefined && $('#boletins').isDataTable()) {
            $('#boletins').DataTable().destroy();
        }

        if ($('#boletinsPendentes') !== undefined &&
            $('#boletinsPendentes').isDataTable()) {
            $('#boletinsPendentes').DataTable().destroy();
        }

        if ($('#boletimDetail') !== undefined &&
            $('#boletimDetail').isDataTable()) {
            $('#boletimDetail').DataTable().destroy();
        }

        if ($('#boletimDetailPendentes') !== undefined &&
            $('#boletimDetailPendentes').isDataTable()) {
            $('#boletimDetailPendentes').DataTable().destroy();
        }

        $('#showLoadModal').show();
        BoletimCargaService.sendFinished(null, null, null);

        BoletimCargaService.findVesselVisitInfo(vm.searchParams, function(
            success) {
            vm.vesselPesq = vm.vessel;

            $('#showLoadModal').hide();
            vm.boletins = success;

            for (var i = 0; i < vm.boletins.length; i++) {
                if (vm.boletins[i].status == " " &&
                    vm.boletins[i].qtdeItens == "50" &&
                    vm.boletins[i].boletimId !== "PENDENCIAS") {
                    BoletimCargaService.generateXML(vm.boletins[i],
                        function(success) {

                        },
                        function(error) {
                            $('#showLoadModal').hide();
                            $("body").css("cursor", "");
                            vm.showDialog = true;
                            vm.errorMessage = error.data.replace(
                                /(\"|\\n)/gi, "<br>");
                        });
                }
            }
        }, function(error) {
            $('#showLoadModal').hide();
            $("body").css("cursor", "");
            vm.showDialog = true;
            vm.errorMessage = error.data.replace(/(\"|\\n)/gi, "<br>");
        });
    };

    function detectarEcpf() {
        BoletimCargaService.detectarEcpf(null, function(success) {
            var retorno = success[0];
            if (retorno.indexOf("t") > -1) {
                vm.hasEcpf = "Detectado!";

                BoletimCargaService.detectarEcpfNbr(null, function(success) {
                    vm.ecpfNbr = success[0];
                    for (var i = 1; i < 11; i++) {
                        if (i === 3 || i === 6)
                            vm.ecpfNbr += "."
                        if (i === 9)
                            vm.ecpfNbr += "-"
                        vm.ecpfNbr += success[i];
                    }
                }, function(error) {
                    $("body").css("cursor", "");
                    vm.showDialog = true;
                    vm.errorMessage = error.data.replace(/(\"|\\n)/gi,
                        "<br>");
                });
            } else {
                vm.hasEcpf = "Não Detectado!";
                vm.ecpfNbr = "0";
            }
        }, function(error) {
            $("body").css("cursor", "");
            vm.showDialog = true;
            vm.errorMessage = error.data.replace(/(\"|\\n)/gi, "<br>");
        });
    };

    // vm.detectarEcpf();
    // vm.pesquisarVesselVisit();
    // vm.pesquisarSearchFields();


    function to_trusted(html_code) {
        // return $sce.trustAsHtml(html_code);
    };


    function closeDialog() {
        vm.showDialog = false;
    };

    function closeDialogSuccess() {
        vm.showDialogSuccess = false;
    };

    function formatChildRow() {
        // `d` is the original data object for the row
        var retorno = "";

        if (vm.OpenRow.boletimId === "PENDENCIAS") {
            retorno = '<table class="table table-bordered boletimDetailPendentes" id="boletimDetailPendentes">' +
                '<thead>' + '<tr>';
        } else {
            retorno = '<table class="table table-bordered boletimDetail" id="boletimDetail">' +
                '<thead>' + '<tr>';
        }

        retorno = retorno + '<th>' + ' ' + '</th>' + '<th>' + 'Unit Nbr' +
            '</th>';

        if (vm.OpenRow.boletimId === "PENDENCIAS") {
            retorno = retorno +
                '<th ng-show="boletim.boletimId == \'PENDENCIAS\'">' +
                'Opera&ccedil;&atilde;o' + '</th>' +
                '<th ng-show="boletim.boletimId == \'PENDENCIAS\'">' +
                'Restow' + '</th>';
        }

        retorno = retorno + '<th>' + 'Seals' + '</th>' + '<th>' + 'Cargo Qty.' +
            '</th>' + '<th>' + 'Freight Kind' + '</th>' + '<th>' +
            'Manifesto' + '</th>' + '<th>' + 'CE' + '</th>' + '<th>' +
            'Item' + '</th>' + '<th>' + 'Damage' + '</th>' + '<th>' +
            'Status' + '</th>';

        if (vm.OpenRow.boletimId === "PENDENCIAS") {
            retorno = retorno + '<th>' + '</th>';
        }

        retorno = retorno + '</tr>' + '</thead>' + '<tbody>';

        for (var i = 0; i < vm.OpenRow.units.length; i++) {
            retorno = retorno + '<tr class="packageInfo">' +
                '<td width="5%" align="center" class=" log-control">' +
                '<i class="icon-plus icon-black iconchild">' + '</i>' +
                '</td>';

            retorno = retorno + '<td width="15%" align="center">' +
                vm.OpenRow.units[i].unitNbr + '</td>';

            if (vm.OpenRow.boletimId === "PENDENCIAS") {
                retorno = retorno +
                    '<td width="15%" align="center" ng-show="boletim.boletimId == \'PENDENCIAS\'">' +
                    vm.OpenRow.units[i].operacao +
                    '</td>' +
                    '<td width="15%" align="center" ng-show="boletim.boletimId == \'PENDENCIAS\'">' +
                    vm.OpenRow.units[i].restow + '</td>';
            }

            retorno = retorno + '<td width="15%" align="center">' +
                vm.OpenRow.units[i].seals + '</td>' +
                '<td width="10%" align="center">' +
                vm.OpenRow.units[i].cargoQty + '</td>' +
                '<td width="10%" align="center">' +
                vm.OpenRow.units[i].freightKind + '</td>' +
                '<td width="10%" align="center">' +
                vm.OpenRow.units[i].manifesto + '</td>' +
                '<td width="10%" align="center">' +
                vm.OpenRow.units[i].ceMercante + '</td>' +
                '<td width="5%" align="center">' +
                vm.OpenRow.units[i].itemSiscomex + '</td>' +
                '<td width="5%" align="center">' +
                vm.OpenRow.units[i].damage + '</td>' +
                '<td width="5%" align="center">' +
                vm.OpenRow.units[i].status + '</td>';

            if (vm.OpenRow.boletimId === "PENDENCIAS") {
                if (vm.OpenRow.units[i].status === "Erro") {
                    retorno = retorno +
                        '<td align="center" class=" details-control">' +
                        '<a class="btn btn-primary btn-sm" onclick="reSend(\'' +
                        vm.OpenRow.units[i].unitBoletimGkey +
                        '\')"' +
                        'style="width: 80px;"> <i class="icon-ok icon-white"></i>&nbsp;' +
                        'Reenviar' + '</a></td>';
                } else {
                    retorno = retorno +
                        '<td align="center" class=" details-control">' +
                        '</td>';
                }
            }

            retorno = retorno + '</tr>';
        }

        retorno = retorno + '</tbody>' + '</table>';

        return retorno;
    };

    function formatChildResultRow() {
        vm.promise = null;

        // `d` is the original data object for the row
        var retorno = '<table class="table table-striped table-bordered table-hover" id="boletimRulesDetail">' +
            '<thead style="font-size: 0.9em;">' +
            '<tr>' +
            '<th>' +
            'Pacote ID' +
            '</th>' +
            '<th>' +
            'Data de Envio' +
            '</th>' +
            '<th>' +
            'Data de Retorno' +
            '</th>' +
            '<th>' +
            'Status' +
            '</th>' + '</tr>' + '</thead>' + '<tbody>';

        for (var i = 0; i < vm.logResult.reportsSent.length; i++) {

            retorno = retorno + '<tr>' + '<td width="25%" align="center">'
                // + '<input type="text" value="'
                +
                vm.logResult.reportsSent[i].packageId
                // + '" class="span12 pull-left" disabled="disabled" />'
                +
                '</td>' + '<td width="25%" align="center">'
                // + '<input type="text" value="'
                +
                vm.logResult.reportsSent[i].dataEnvioFormatada
                // + '" class="span12 pull-left" disabled="disabled" />'
                +
                '</td>' + '<td width="25%" align="center">'
                // + '<input type="text" value="'
                +
                vm.logResult.reportsSent[i].dataRetornoFormatada
                // + '" class="span12 pull-left" disabled="disabled" />'
                +
                '</td>' + '<td width="25%" align="center">'
                // + '<input type="text" value="'
                +
                vm.logResult.reportsSent[i].status
                // + '" class="span12 pull-left" disabled="disabled" />'
                +
                '</td>';

            retorno = retorno + '</tr> <tr> <td  colspan="4">';

            if (vm.logResult.returnDetails !== undefined) {
                for (var j = 0; j < vm.logResult.returnDetails.length; j++) {
                    if (vm.logResult.returnDetails[j].pacoteId === vm.logResult.reportsSent[i].packageId) {
                        retorno = retorno +
                            '<table class="table table-striped table-bordered table-hover" id="boletimRulesDetail">' +
                            '<thead style="font-size: 0.9em;">' + '<tr>' +
                            '<th>' + 'Pacote' + '</th>' + '<th>' +
                            'Descrição' + '</th>' + '</tr>' + '</thead>' +
                            '<tbody>';

                        retorno = retorno + '<tr>' +
                            '<td width="20%" align="center">' +
                            vm.logResult.returnDetails[j].pacoteId +
                            '</td>' + '<td width="80%" align="center">' +
                            vm.logResult.returnDetails[j].descricao +
                            '</td>' + '</tr>';

                        retorno = retorno + '</tbody>' + '</table> </td> </tr>';

                        j = vm.logResult.returnDetails.length;
                    }
                }
            }

            retorno = retorno + '</td>' + '</tr>';
        }

        retorno = retorno + '</tbody>' + '</table>';

        // Regra 1 a 5
        retorno = retorno +
            '<table style="margin-left: 10px !important;" class="table table-striped table-bordered table-hover span4" id="boletimRulesDetail">' +
            '<tbody>';

        retorno = retorno + '<tr>' +
            '<td class="span1" width="10%" align="center">' + '1' +
            '</td>' + '<td class="span10" width="80%" align="center">' +
            'Número da Escala' + '</td>' +
            '<td class="span1" width="10%" align="center">' +
            vm.logResult.unitRules.rule1 + '</td>' + '</tr>';

        retorno = retorno + '<tr>' +
            '<td class="span1" width="10%" align="center">' + '2' +
            '</td>' + '<td class="span10" width="80%" align="center">' +
            'Código do Terminal de Operação' + '</td>' +
            '<td class="span1" width="10%" align="center">' +
            vm.logResult.unitRules.rule2 + '</td>' + '</tr>';

        retorno = retorno + '<tr>' +
            '<td class="span1" width="10%" align="center">' + '3' +
            '</td>' + '<td class="span10" width="80%" align="center">' +
            'Código do Tipo do Item' + '</td>' +
            '<td class="span1" width="10%" align="center">' +
            vm.logResult.unitRules.rule3 + '</td>' + '</tr>';

        retorno = retorno + '<tr>' +
            '<td class="span1" width="10%" align="center">' + '4' +
            '</td>' + '<td class="span10" width="80%" align="center">' +
            'Contêiner' + '</td>' +
            '<td class="span1" width="10%" align="center">' +
            vm.logResult.unitRules.rule4 + '</td>' + '</tr>';

        retorno = retorno + '<tr>' +
            '<td class="span1" width="10%" align="center">' + '5' +
            '</td>' + '<td class="span10" width="80%" align="center">' +
            'Número do Contêiner' + '</td>' +
            '<td class="span1" width="10%" align="center">' +
            vm.logResult.unitRules.rule5 + '</td>' + '</tr>';

        retorno = retorno + '</tbody>' + '</table>';

        // Regra 6 a 10
        retorno = retorno +
            '<table style="margin-left: 10px !important;" class="table table-bordered table-striped table-hover span4" id="boletimRulesDetail">' +
            '<tbody>';

        retorno = retorno + '<tr>' +
            '<td class="span1" width="10%" align="center">' + '6' +
            '</td>' + '<td class="span10" width="80%" align="center">' +
            'Nro. de Lacres do Contêiner' + '</td>' +
            '<td class="span1" width="10%" align="center">' +
            vm.logResult.unitRules.rule6 + '</td>' + '</tr>';

        retorno = retorno + '<tr>' +
            '<td class="span1" width="10%" align="center">' + '7' +
            '</td>' + '<td class="span10" width="80%" align="center">' +
            'Nro. do Lacre do Contêiner' + '</td>' +
            '<td class="span1" width="10%" align="center">' +
            vm.logResult.unitRules.rule7 + '</td>' + '</tr>';

        retorno = retorno + '<tr>' +
            '<td class="span1" width="10%" align="center">' + '8' +
            '</td>' + '<td class="span10" width="80%" align="center">' +
            'Carga Solta' + '</td>' +
            '<td class="span1" width="10%" align="center">' +
            vm.logResult.unitRules.rule8 + '</td>' + '</tr>';

        retorno = retorno + '<tr>' +
            '<td class="span1" width="10%" align="center">' + '9' +
            '</td>' + '<td class="span10" width="80%" align="center">' +
            'Qtde. de Carga Solta Operada' + '</td>' +
            '<td class="span1" width="10%" align="center">' +
            vm.logResult.unitRules.rule9 + '</td>' + '</tr>';

        retorno = retorno + '<tr>' +
            '<td class="span1" width="10%" align="center">' + '10' +
            '</td>' + '<td class="span10" width="80%" align="center">' +
            'Indicador de Avaria de Carga' + '</td>' +
            '<td class="span1" width="10%" align="center">' +
            vm.logResult.unitRules.rule10 + '</td>' + '</tr>';

        retorno = retorno + '</tbody>' + '</table>';

        // Regra 11 a 15
        retorno = retorno +
            '<table style="margin-left: 10px !important;" class="table table-striped table-bordered table-hover span4" id="boletimRulesDetail">' +
            '<tbody>';

        retorno = retorno + '<tr>' +
            '<td class="span1" width="10%" align="center">' + '11' +
            '</td>' + '<td class="span10" width="80%" align="center">' +
            'Nro. do Manifesto' + '</td>' +
            '<td class="span1" width="10%" align="center">' +
            vm.logResult.unitRules.rule11 + '</td>' + '</tr>';

        retorno = retorno + '<tr>' +
            '<td class="span1" width="10%" align="center">' + '12' +
            '</td>' + '<td class="span10" width="80%" align="center">' +
            'Nro. do Conhecimento' + '</td>' +
            '<td class="span1" width="10%" align="center">' +
            vm.logResult.unitRules.rule12 + '</td>' + '</tr>';

        retorno = retorno + '<tr>' +
            '<td class="span1" width="10%" align="center">' + '13' +
            '</td>' + '<td class="span10" width="80%" align="center">' +
            'Nro. do Item' + '</td>' +
            '<td class="span1" width="10%" align="center">' +
            vm.logResult.unitRules.rule13 + '</td>' + '</tr>';

        retorno = retorno + '<tr>' +
            '<td class="span1" width="10%" align="center">' + '14' +
            '</td>' + '<td class="span10" width="80%" align="center">' +
            'Código da Operação' + '</td>' +
            '<td class="span1" width="10%" align="center">' +
            vm.logResult.unitRules.rule14 + '</td>' + '</tr>';

        retorno = retorno + '<tr>' +
            '<td class="span1" width="10%" align="center">' + '15' +
            '</td>' + '<td class="span10" width="80%" align="center">' +
            'Código da Movimentação' + '</td>' +
            '<td class="span1" width="10%" align="center">' +
            vm.logResult.unitRules.rule15 + '</td>' + '</tr>';

        retorno = retorno + '</tbody>' + '</table>';

        return retorno;
    };

    function addChildRow(boletim) {
        // vm.OpenRow = boletim;
        vm.boletins.push({ boletimId: 1, dataRetornoFormatada: '04/12/2017', operacao: 'AA', restow: 'AA', status: 'Cancelado' });
    };

    function addLogResult(pos) {
        vm.promise = vm.callLogResult(pos);
    };

    function callLogResult(pos) {
        $('#showLoadModal').show();
        return BoletimCargaService.findLogInfo(
            vm.OpenRow.units[pos].unitBoletimGkey.toString(),
            function(
                success) {
                $('#showLoadModal').hide();
                vm.logResult = success;
            },
            function(error) {
                $('#showLoadModal').hide();
                $("body").css("cursor", "");
                vm.showDialog = true;
                vm.errorMessage = error.data.replace(/(\"|\\n)/gi,
                    "<br>");
            });
    };

    function applyEvents() {
        // Add event listener for opening and closing details
        $('#boletins tbody')
            .on(
                'click',
                'td.details-control',
                function() {
                    if ($("#boletins").isDataTable()) {
                        var table = $("#boletins").DataTable();
                    } else {
                        var table = $("#boletins").DataTable({
                            paging: false,
                            "bInfo": false,
                            "bLengthChange": false,
                            "ordering": false,
                            "bFilter": false,
                            "autoWidth": false,
                            "bAutoWidth": false,
                            // "scrollX": true,
                        });
                    }

                    var tr = $(this).closest('tr');
                    var row = table.row(tr);
                    var i = $($('.iconparent')[parseInt(row[0]) + vm.boletins.length]);

                    if (row.child.isShown()) {
                        row.child.hide();
                        tr.removeClass('shown');
                        i.removeClass("icon-minus");
                        i.addClass('icon-plus');
                    } else {
                        if ($(".packages.shown").length !== 0) {
                            var temp = $($(".packages.shown")[parseInt(0)]);
                            var rowTemp = table.row(temp);
                            var iTemp = $($('.iconparent')[parseInt(rowTemp[0])]);

                            rowTemp.child.hide();
                            temp.removeClass('shown');
                            iTemp.removeClass("icon-minus");
                            iTemp.addClass('icon-plus');
                        }

                        var retorno = vm.formatChildRow(row.data());

                        // Open this row
                        row.child(retorno).show();

                        // Add event listener for opening and closing
                        // details
                        $('#boletimDetail tbody')
                            .on(
                                'click',
                                'td.log-control',
                                function() {
                                    if ($(this).closest(
                                            '.boletimDetail')
                                        .isDataTable()) {
                                        var table = $(this)
                                            .closest(
                                                '.boletimDetail')
                                            .DataTable();
                                    } else {
                                        var table = $(this)
                                            .closest(
                                                '.boletimDetail')
                                            .DataTable({
                                                paging: false,
                                                "bInfo": false,
                                                "bLengthChange": false,
                                                "ordering": false,
                                                "bFilter": false,
                                                "autoWidth": false,
                                                "bAutoWidth": false,
                                                // "scrollX":
                                                // true,
                                            });
                                    }
                                    var tr = $(this).closest(
                                        'tr');
                                    var row = table.row(tr);
                                    var i = $($(this).closest(
                                            '.log-control')
                                        .find('i'));

                                    if (row.child.isShown()) {
                                        row.child.hide();
                                        tr.removeClass('shown');
                                        i
                                            .removeClass('icon-minus');
                                        i.addClass('icon-plus');
                                    } else {
                                        if ($(".packageInfo.shown").length !== 0) {
                                            var temp = $($(".packageInfo.shown")[parseInt(0)]);
                                            var rowTemp = table
                                                .row(temp);
                                            var iTemp = $($(
                                                    $(temp)
                                                    .find(
                                                        '.log-control'))
                                                .find('i'));

                                            rowTemp.child
                                                .hide();
                                            temp
                                                .removeClass('shown');
                                            iTemp
                                                .removeClass("icon-minus");
                                            iTemp
                                                .addClass('icon-plus');
                                        }

                                        vm.addLogResult(row
                                            .index());

                                        // Open this row
                                        vm.promise.$promise
                                            .then(function() {
                                                var retorno = vm
                                                    .formatChildResultRow(row
                                                        .data());

                                                row
                                                    .child(
                                                        retorno)
                                                    .show();
                                                tr
                                                    .addClass('shown');
                                                i
                                                    .addClass('icon-minus');
                                                i
                                                    .removeClass('icon-plus');
                                            });
                                    }
                                });

                        tr.addClass('shown');
                        i.addClass('icon-minus');
                        i.removeClass('icon-plus');
                    }
                });

        // Add event listener for opening and closing details
        $('#boletinsPendentes tbody')
            .on(
                'click',
                'td.details-control',
                function() {
                    if ($("#boletinsPendentes").isDataTable()) {
                        var table = $("#boletinsPendentes").DataTable();
                    } else {
                        var table = $("#boletinsPendentes").DataTable({
                            paging: false,
                            "bInfo": false,
                            "bLengthChange": false,
                            "ordering": false,
                            "bFilter": false,
                            "autoWidth": false,
                            "bAutoWidth": false,
                            // "scrollX": true,
                        });
                    }

                    var tr = $(this).closest('tr');
                    var row = table.row(tr);
                    var i = $($('.iconparent')[parseInt(row[0])]);

                    if (row.child.isShown()) {
                        row.child.hide();
                        tr.removeClass('shown');
                        i.removeClass("icon-minus");
                        i.addClass('icon-plus');
                    } else {
                        if ($(".packages.shown").length !== 0) {
                            var temp = $($(".packages.shown")[parseInt(0)]);
                            var rowTemp = table.row(temp);
                            var iTemp = $($('.iconparent')[parseInt(rowTemp[0])]);

                            rowTemp.child.hide();
                            temp.removeClass('shown');
                            iTemp.removeClass("icon-minus");
                            iTemp.addClass('icon-plus');
                        }

                        var retorno = vm.formatChildRow(row.data());

                        // Open this row
                        row.child(retorno).show();

                        // Add event listener for opening and closing
                        // details
                        $('#boletimDetailPendentes tbody')
                            .on(
                                'click',
                                'td.log-control',
                                function() {
                                    if ($(this)
                                        .closest(
                                            '.boletimDetailPendentes')
                                        .isDataTable()) {
                                        var table = $(this)
                                            .closest(
                                                '.boletimDetailPendentes')
                                            .DataTable();
                                    } else {
                                        var table = $(this)
                                            .closest(
                                                '.boletimDetailPendentes')
                                            .DataTable({
                                                paging: false,
                                                "bInfo": false,
                                                "bLengthChange": false,
                                                "ordering": false,
                                                "bFilter": false,
                                                "autoWidth": false,
                                                "bAutoWidth": false,
                                                // "scrollX":
                                                // true,
                                            });
                                    }
                                    var tr = $(this).closest(
                                        'tr');
                                    var row = table.row(tr);
                                    var i = $($(this).closest(
                                            '.log-control')
                                        .find('i'));

                                    if (row.child.isShown()) {
                                        row.child.hide();
                                        tr.removeClass('shown');
                                        i
                                            .removeClass('icon-minus');
                                        i.addClass('icon-plus');
                                    } else {
                                        if ($(".packageInfo.shown").length !== 0) {
                                            var temp = $($(".packageInfo.shown")[parseInt(0)]);
                                            var rowTemp = table
                                                .row(temp);
                                            var iTemp = $($(
                                                    $(temp)
                                                    .find(
                                                        '.log-control'))
                                                .find('i'));

                                            rowTemp.child
                                                .hide();
                                            temp
                                                .removeClass('shown');
                                            iTemp
                                                .removeClass("icon-minus");
                                            iTemp
                                                .addClass('icon-plus');
                                        }

                                        vm.addLogResult(row
                                            .index());

                                        // Open this row
                                        vm.promise.$promise
                                            .then(function() {
                                                var retorno = vm
                                                    .formatChildResultRow(row
                                                        .data());

                                                row
                                                    .child(
                                                        retorno)
                                                    .show();
                                                tr
                                                    .addClass('shown');
                                                i
                                                    .addClass('icon-minus');
                                                i
                                                    .removeClass('icon-plus');
                                            });
                                    }
                                });

                        tr.addClass('shown');
                        i.addClass('icon-minus');
                        i.removeClass('icon-plus');
                    }
                });
    }

    function startInterval() {
        // $interval(
        //     function() {
        //         $('#showLoadModal').show();

        //         Liferay.Session.extend();

        //         if (vm.searchParams === undefined) {
        //             $('#showLoadModal').hide();
        //             return;
        //         }

        //         BoletimCargaService.sendFinished(null, null, null);

        //         BoletimCargaService
        //             .findVesselVisitInfo(
        //                 vm.searchParams,
        //                 function(success) {
        //                     vm.vesselPesq = vm.vessel;

        //                     vm.boletins = success;

        //                     if ($('#boletins') !== undefined &&
        //                         $('#boletins').isDataTable()) {
        //                         $('#boletins').DataTable()
        //                             .destroy();
        //                     }

        //                     if ($('#boletinsPendentes') !== undefined &&
        //                         $('#boletinsPendentes')
        //                         .isDataTable()) {
        //                         $('#boletinsPendentes').DataTable()
        //                             .destroy();
        //                     }

        //                     if ($('#boletimDetail') !== undefined &&
        //                         $('#boletimDetail')
        //                         .isDataTable()) {
        //                         $('#boletimDetail').DataTable()
        //                             .destroy();
        //                     }

        //                     if ($('#boletimDetailPendentes') !== undefined &&
        //                         $('#boletimDetailPendentes')
        //                         .isDataTable()) {
        //                         $('#boletimDetailPendentes')
        //                             .DataTable().destroy();
        //                     }

        //                     for (var i = 0; i < vm.boletins.length; i++) {
        //                         if (vm.boletins[i].status == " " &&
        //                             vm.boletins[i].qtdeItens == "50" &&
        //                             vm.boletins[i].boletimId !== "PENDENCIAS") {
        //                             BoletimCargaService
        //                                 .generateXML(
        //                                     vm.boletins[i],
        //                                     function(
        //                                         success) {

        //                                     },
        //                                     function(error) {
        //                                         $(
        //                                                 '#showLoadModal')
        //                                             .hide();
        //                                         $("body")
        //                                             .css(
        //                                                 "cursor",
        //                                                 "");
        //                                         vm.showDialog = true;
        //                                         vm.errorMessage = error.data
        //                                             .replace(
        //                                                 /(\"|\\n)/gi,
        //                                                 "<br>");
        //                                     });
        //                         }
        //                     }

        //                     $('#showLoadModal').hide();
        //                 })
        //     }, 300000);
    }

    // vm.applyEvents();
    // vm.startInterval();

}]);