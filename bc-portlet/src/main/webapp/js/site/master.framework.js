﻿var __datatable_SearchTimedFunction;
var teste = "aa";

function ValidationColor() {
	$(".inputRequired").off("keyup");
	$(".inputRequired").on('keyup', function () { $(this).removeClass("inputRequired"); $(this).off("keyup"); });
}

function __gridFilter(obj) {
	var aux = obj.id;
	var auxSplit = aux.split("_spt_");
	if (auxSplit.length >= 3) {
		var tableAux = $("#" + auxSplit[1]);
		var table = $("#" + auxSplit[1]).DataTable();

		if (__datatable_SearchTimedFunction != undefined && __datatable_SearchTimedFunction != null) {
			window.clearTimeout(__datatable_SearchTimedFunction);
		}
		__datatable_SearchTimedFunction = setTimeout(function () {
			table.draw()
			//$("#" + table.context[0].sTableId).DataTable().draw();
		}, table.context[0].oInit.fw_options.columnFilterMilliseconds);
	}
}


function ConvertToJSON(data, paramRequestName, setAsJSON) {
	var seen = [];
	var jsonData = JSON.stringify(data, function (key, val) {
		if (typeof val == "object") {
			if ($.inArray(val, seen) >= 0)
				return undefined;
			seen.push(val);
		}
		return val;
	});
	if (paramRequestName == "" || paramRequestName === undefined) paramRequestName = "request";
	if (!setAsJSON) {
		return "{ " + paramRequestName + ": " + jsonData + " }";
	}
	else {
		return paramRequestName + "=" + jsonData;
	}
};

; (function ($) {
	"use strict";
	$.fn.GetDataTableSearchCount = function () {
		try {
			return $(this).DataTable().context[0].oInit.fw_options.tableSearchCount;
		}
		catch (e) { return 0; }
	};
	$.fn.GetDataTableIsFiltered = function () {
		try {
			return $(this).DataTable().context[0].oInit.fw_options.tableFiltered;
		}
		catch (e) { return false; }
	};
	$.fn.DTRefresh = function () {
		$(this).DataTable().draw();
		if ($(this).parent() != null && $(this).parent() != undefined) {
			if ($(this).closest(".areaGrid").hasClass("datatable_loadinvisible")) {
				$(this).closest(".areaGrid").removeClass("datatable_loadinvisible");
			}
		}
	};
	$.fn.ClearDataTable = function () {
		$(this).dataTable().empty();
		$(this).dataTable().fnDestroy();
		$(this).closest(".areaGrid").addClass("datatable_loadinvisible");
	}
	$.fn.isDataTable = function () {
		var nTable = $(this)[0];
		var settings = $.fn.dataTableSettings;
		if (settings != null || settings != undefined)
			{
				for (var i = 0, iLen = settings.length ; i < iLen ; i++) {
					if (settings[i].nTable == nTable) {
						return true;
					}
				}
			}
		return false;
	};
	$.fn.GetDataTableInfo = function () {
		var ret = {};
		ret.sort = {};
		ret.filters = {};
		var filters = {};
		var fw_options = $($(this)[0]).DataTable().context[0].oInit.fw_options;

		if (fw_options.filters != null || fw_options.filters != undefined) {
			filters = fw_options.filters();
			$(filters).each(function (index) {
				filters[index].name = fw_options.filter_prefix + filters[index].name;
				if (filters[index].desc == undefined || filters[index].desc == null) {
					filters[index].desc = filters[index].name;
				}
			});
		}

		$(".column_filter", $($(this)[0])).each(function (index) {
			var itens = this.id.split('_spt_');
			if (itens.length >= 2) {
				var datatableAux = $("#" + itens[1]).DataTable().context[0];
				if ($(this).val() != "") {
					try {
						if (itens[0] == "column_filters_for") {
							filters.push({ name: fw_options.filter_prefix + datatableAux.aoColumns[itens[2]].data, value: $(this).val(), desc: datatableAux.aoColumns[itens[2]].sTitle });
						}
					} catch (e) {
						alert("Falha ao pegar o contexto do datatable. (.column_filter)");
					}
				}
				ret.sort.desc = datatableAux.aoColumns[datatableAux.aLastSort[0].col].sTitle;
				ret.sort.data = datatableAux.aoColumns[datatableAux.aLastSort[0].col].data;
				ret.sort.dir = datatableAux.aLastSort[0].dir;
			}
		});
		ret.filters = filters;
		ret.page = $($(this)[0]).DataTable().page.info();
		return ret;
	};
	$.fn.CreateDataTable = function (ajaxURI, columns, options) {
		if (options == null && options == undefined) {
			options = {};
		}

		if (options.tableSearchCount == null || options.tableSearchCount == undefined) {
			options.tableSearchCount = 0;
		}
		if (options.tableFiltered == null || options.tableFiltered == undefined) {
			options.tableFiltered = false;
		}

		if (options.checkIfIsDataTable == null || options.checkIfIsDataTable == undefined) {
			options.checkIfIsDataTable = true;
		}

		if (options.makevisible == null || options.makevisible == undefined) {
			options.makevisible = true;
		}

		if (options.checkIfIsDataTable && this.isDataTable()) { return; }

		var select = $(this);
		var id = $(this)[0].id;
		if (options.debugMode_AlertData == null || options.debugMode_AlertData == undefined) {
			options.debugMode_AlertData = false;
		}
		if (options.filter_prefix == null || options.filter_prefix == undefined) {
			options.filter_prefix = "filter|";
		}
		if (options.columnFilterMilliseconds == null || options.columnFilterMilliseconds == undefined) {
			options.columnFilterMilliseconds = 500;
		}
		if (options.ajaxSendAsJSON == null || options.ajaxSendAsJSON == undefined) {
			options.ajaxSendAsJSON = false;
		}
		if (options.ajaxSendAsJSONAsParameter == null || options.ajaxSendAsJSONAsParameter == undefined) {
			options.ajaxSendAsJSONAsParameter = true;
		}
		if (options.ajaxSendParameterName == null && options.ajaxSendParamterName == undefined) {
			options.ajaxSendParameterName = "data";
		}

		if (options.filterPosition == null || options.filterPosition == undefined) {
			options.filterPosition = "none";
		}

		if (options.bottomSearch) {
			options.filterPosition = "bottom";
		}

		if (options.ZeroRecords == null || options.ZeroRecords == undefined) {
			options.ZeroRecords = "Nenhum registro encontrado";
		}


		switch (options.filterPosition) {
		case "top":
			var thead = options.theadfind;
			var watermark = options.watermark;
			var isTitleInWatermark = options.isTitleInWatermark;
			if (thead == null || thead == undefined) {
				thead = "#" + id + " thead th";
			}
			if (watermark == null || watermark == undefined) {
				watermark = "Buscar ";
			}
			if (isTitleInWatermark == null || isTitleInWatermark == undefined) {
				isTitleInWatermark = true;
			}
			$(thead).each(function () {
				if ($(this).hasClass("column_filter_th")) {
					var title = $(thead).eq($(this).index()).text();
					$(this).html('<input type="text" id="column_filters_for_spt_' + id + '_spt_' + $(this).index() + '" class="column_filter" placeholder="' + watermark + (isTitleInWatermark ? title : "") + '" onkeyup="__gridFilter(this);" /><br />');
				}
			});
			break;
		case "bottom":
			var tfoot = options.tfootfind;
			var thead = options.theadfind;
			var watermark = options.watermark;
			var isTitleInWatermark = options.isTitleInWatermark;
			if (tfoot == null || tfoot == undefined) {
				tfoot = "#" + id + " tfoot th";
			}
			if (thead == null || thead == undefined) {
				thead = "#" + id + " thead th";
			}
			if (watermark == null || watermark == undefined) {
				watermark = "Buscar ";
			}
			if (isTitleInWatermark == null || isTitleInWatermark == undefined) {
				isTitleInWatermark = true;
			}
			$(tfoot).each(function () {
				var title = $(thead).eq($(this).index()).text();
				$(this).html('<input type="text" id="column_filters_for_spt_' + id + '_spt_' + $(this).index() + '" class="column_filter" placeholder="' + watermark + (isTitleInWatermark ? title : "") + '" onkeyup="__gridFilter(this);" />');
			});
			break;
		case "none":
			break;
		}


		if (options.lengthMenu == null && options.lengthMenu == undefined) {
			options.lengthMenu = [[10, 25, 50, 100], [10, 25, 50, 100]];
		}

		var columnsArray = [];

		$(columns).each(function (index) {
			if(typeof this === "string"){
				columnsArray.push({ "data": this }); 
			}else{
				columnsArray.push(this);
			}
		});

		var newoptions = {
				"fw_options": options,
				"searching": false,
				"processing": (options.processing == null && options.processing == undefined ? true : options.processing),
				"serverSide": (options.serverSide == null && options.serverSide == undefined ? true : options.serverSide),
				"ajax": {
					"url": ajaxURI,
					"type": (options.ajaxType == null && options.ajaxType == undefined ? "POST" : options.ajaxType),
					"dataType": "json",
	                "dataSrc": function (json) {
	                    if (json.draw == undefined) {
	                        json = {
	                            draw: 1,
	                            recordsFiltered: json.length,
	                            recordsTotal: json.length,
	                            data: json
	                        };
	                    }
	                    return json.data;
	                },
					"data": function (data) {
						if (options.ajaxSendData != null && options.ajaxSendData != undefined) {
							return options.ajaxSendData(data);
						}
						if (options.filters != null || options.filters != undefined) {
							data.filters = options.filters();

							$(data.filters).each(function (index) {
								data.filters[index].name = options.filter_prefix + data.filters[index].name;
							});
						}
						$(".column_filter", $("#" + id)).each(function (index) {
							var idtext = $(this).attr("id");
							var vltext = $("#" + idtext).val();                        

							if (vltext != "") {
								var itens = idtext.split('_spt_');

								if (itens.length >= 2) {
									try {
										var datatableAux = $("#" + itens[1]).DataTable().context[0];
										if (itens[0] == "column_filters_for") {
											datatableAux.oInit.fw_options.tableFiltered = true;
											data.filters.push({ name: options.filter_prefix + datatableAux.aoColumns[itens[2]].data, value: vltext });
										}
									} catch (e) {
										alert("Falha ao pegar o contexto do datatable. (.column_filter)");
									}
								}
							}
							try {
								if ($.browser.versionNumber != "8") {
									$(this).attr('readonly', true);
								}
							} catch (e) {
								alert("erro");
							}
						});

						var sortFields = "";
						var sortOrders = "";

						if (data.order != null && data.order != undefined && data.order.length > 0) {
							//caso seja necessário implementar multiplos campos de ordenação, mudar a linha abaixo
							//hardcoded para apenas 1 campo de ordenação por vez
							sortFields = data.columns[data.order[0].column].data;
							sortOrders = data.order[0].dir;
						}

						if (options.debugMode_AlertData) {
							var debugAlertFilter = "";
							$(data.filters).each(function (index) {
								debugAlertFilter = debugAlertFilter + "\n" + "Name: " + data.filters[index].name + " - Value: " + data.filters[index].value;
							});
							debugAlertFilter = debugAlertFilter + "\n\n" + "SortField: " + sortFields + " - SortOrder: " + sortOrders;
							alert(debugAlertFilter);
						}

						data.sortField = sortFields;
						data.sortOrder = sortOrders;
						if (options.ajaxSendAsJSON) {
							return ConvertToJSON(data, options.ajaxSendParameterName, options.ajaxSendAsJSONAsParameter);
						}
						else {
							return data;
						}
					}
				},
				"bJQueryUI": (options.bJQueryUI == null && options.bJQueryUI == undefined ? true : options.bJQueryUI),
				"columns": columnsArray,
				"oLanguage": {
					"sEmptyTable": options.ZeroRecords,
					"sInfo": "_START_/_END_ de _TOTAL_ registros",
					"sInfoEmpty": "0 Registros",
					"sInfoFiltered": "(Filtrar de _MAX_ total registros)",
					"sInfoPostFix": "",
					"sInfoThousands": ".",
					"sLengthMenu": "Mostrar _MENU_ registros por pagina",
					"sLoadingRecords": "Carregando...",
					"sProcessing": "Processando...",
					"sZeroRecords": options.ZeroRecords,
					"sSearch": "Pesquisar",
					"oPaginate": {
						"sNext": "Próximo",
						"sPrevious": "Anterior",
						"sFirst": "Primeiro",
						"sLast": "Ultimo"
					},
					"oAria": {
						"sSortAscending": ": Ordenar colunas de forma ascendente",
						"sSortDescending": ": Ordenar colunas de forma descendente"
					}
				}
		};

		$.extend(true, newoptions, options);


		return $(this)
		.on('draw.dt', function () {
			$(".column_filter", $(this)).each(function (index) {
				$(this).removeAttr('readonly');
			});

			$(this).closest(".datatable_loadinvisible").removeClass("datatable_loadinvisible");

			if (!$(this).GetDataTableIsFiltered() && $(this).DataTable().context[0].oInit.fw_options != null)
			{                    
				$(this).DataTable().context[0].oInit.fw_options.tableSearchCount = $(this).dataTable().fnSettings().fnRecordsTotal();
			}
		})
		.on('preXhr.dt', function (e, settings, data) {

		})
		.dataTable(newoptions);

		

	};

})(jQuery);
