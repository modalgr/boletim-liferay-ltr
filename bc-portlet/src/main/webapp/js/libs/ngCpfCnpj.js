/* global angular, CPF, CNPJ */
(function(window){

  'use strict';

  var module = angular.module('ngCpfCnpj', []);

  if( window.CPF ) {

    module.directive('ngCpf', function() {
      return {

        restrict: 'A',

        require: 'ngModel',
        
        link: function(scope, elm, attrs, ctrl) {
        	
        	//watch ngCpf property of isolated scope (bound ngCpf attribute)
        	scope.$watch(attrs.ngCpf, validateCPF);
        	//watch ngModel property of isolated scope (bound ngModel attribute)
        	scope.$watch(attrs.ngModel, validateCPF);
        	
        	function validateCPF(value) {
        		var ngModelVal = scope.$eval(attrs.ngModel);
        		var ngCpfVal = scope.$eval(attrs.ngCpf);
        		
        		//if model is empty doesn't validate (always valid)
        		//in case attribute ngCpf is empty, validates. If it's not empty, check for boolean expression
	        	if(ngModelVal && (!attrs.ngCpf || ngCpfVal === true)) {
	        		ctrl.$setValidity('cpf', CPF.isValid(ngModelVal));
	        	} else {
	        		ctrl.$setValidity('cpf', true);
	        	}
	        }
        }
      };
    });
  }

  if( window.CNPJ ) {

    module.directive('ngCnpj', function() {
      return {

        restrict: 'A',

        require: 'ngModel',

        link: function(scope, elm, attrs, ctrl) {
	        //watch ngCpf property of isolated scope (bound ngCpf attribute)
	      	scope.$watch(attrs.ngCnpj, validateCNPJ);
	      	//watch ngModel property of isolated scope (bound ngModel attribute)
	      	scope.$watch(attrs.ngModel, validateCNPJ);
          
          
	      	function validateCNPJ (value) {
	      		var ngModelVal = scope.$eval(attrs.ngModel);
        		var ngCnpjVal = scope.$eval(attrs.ngCnpj);
        		
	      		//if model is empty doesn't validate (always valid)
        		//in case attribute ngCnpj is empty, validates. If it's not empty, check for boolean expression
	        	if(ngModelVal && (!attrs.ngCnpj || ngCnpjVal === true)) {
	        		ctrl.$setValidity('cnpj', CNPJ.isValid(ngModelVal));
	        	} else {
	        		ctrl.$setValidity('cnpj', true);
	        	}
	        }
        }

      };
    });
  }

})(this);