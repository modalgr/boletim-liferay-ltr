﻿$.datepicker.regional["ptbr"] = {
	closeText: "Fechar",
	prevText: "Voltar",
	nextText: "Proximo",
	currentText: "Agora",
	monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
	"Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
	monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun",
	"Jul", "Ago", "Set", "Out", "Nov", "Dez"],
	dayNames: ["Domingo", "Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira", "Sábado"],
	dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
	dayNamesMin: ["Do", "Se", "Te", "Qa", "Qi", "Se", "Sa"],
	weekHeader: "Semana",
	dateFormat: "dd/mm/yy",
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: ""
};
$.datepicker.setDefaults($.datepicker.regional["ptbr"]);


$.timepicker.regional["ptbr"] = {
	timeOnlyTitle: "Escolha a hora",
	timeText: "Horário",
	hourText: "Hora",
	minuteText: "Minuto",
	secondText: "Segundos",
	millisecText: "Milisegundos",
	timezoneText: "Fuso Horário",
	currentText: "Agora",
	closeText: "Fechar",
	timeFormat: "HH:mm",
	amNames: ["AM", "A"],
	pmNames: ["PM", "P"],
	isRTL: false
};
$.timepicker.setDefaults($.timepicker.regional["ptbr"]);