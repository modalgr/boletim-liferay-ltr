BoletimApp.service('BoletimCargaService', ['$http', function($http) {

    var pesquisarVesselVisit = function() {
        $http.post("/bcservice/rest/pesquisarVesselVisit");
    }

    var findVesselVisitInfo = function() {
        $http.post("/bcservice/rest/findVesselVisitInfo");
    }

    var findLogInfo = function() {
        $http.post("/bcservice/rest/findLogInfo");
    }

    var generateXML = function() {
        $http.post("/bcservice/rest/generateXML");
    }

    var reGenerateXML = function() {
        $http.post("/bcservice/rest/reGenerateXML");
    }

    var detectarEcpf = function() {
        $http.post("/bcservice/rest/detectarEcpf");
    }

    var pesquisarSearchFields = function(params) {
        $http.post("localhost:9005/rest/cargoReportMovimentacoes/" + params.ID + "/" + params.value);
    }

    var detectarEcpfNbr = function() {
        $http.post("/bcservice/rest/detectarEcpfNbr");
    }

    var sendFinished = function() {
        $http.post("/bcservice/rest/sendFinished");
    }


    this.pesquisarVesselVisit = pesquisarVesselVisit;
    this.findVesselVisitInfo = findVesselVisitInfo;
    this.findLogInfo = findLogInfo;
    this.generateXML = generateXML;
    this.reGenerateXML = reGenerateXML;
    this.detectarEcpf = detectarEcpf;
    this.pesquisarSearchFields = pesquisarSearchFields;
    this.detectarEcpfNbr = detectarEcpfNbr;
    this.sendFinished = sendFinished;

}]);