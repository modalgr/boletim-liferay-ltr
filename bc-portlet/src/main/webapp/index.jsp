<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
    <%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
        <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
            <%-- <%@ page import="br.com.fcamara.framework.web.util.LiferayUtil"%> --%>
                <%@ page import="com.liferay.portal.util.PortalUtil" %>
                    <%@ page import="java.util.List"%>
                        <%@ page import="java.util.ArrayList"%>
                            <%@ page import="java.util.Arrays"%>

                                <liferay-theme:defineObjects />
                                <portlet:defineObjects />

                                <%
  Boolean isUserLibra = false;
  List<String> allowed = new ArrayList<String>();
  allowed = Arrays.asList("Analista Comercial","Assistente Armazem",
			"Assistente Faturamento","Assistente Prontificacao","Especialista Aduaneiro",
			"Gerente Armazem","Supervisor Planejamento","Gerente Planejamento","Prontificador","Planejador",
			"Supervisor Armazem", "Assistente San");
  //if (LiferayUtil.hasGroup(user, allowed)) {
	  isUserLibra = true;
//  }
  
  Boolean hasPermissionAcessoAplicacao = false;
	List<String> allowedReservaJanela = new ArrayList<String>();
	allowedReservaJanela = Arrays.asList("Prontificador","Planejador", "Assistente San");

//	hasPermissionAcessoAplicacao = LiferayUtil.hasGroup(user, allowedReservaJanela);

	
%>

                                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

                                    <!-- Jquery -->
                                    <script src="/boletimCarga-portlet/Content/js/jquery3.2.1/jquery-3.2.1.min.js"></script>
                                    <!-- BOOTSTRAP JS -->
                                    <link href="/boletimCarga-portlet/css/grupolibra/boletim.css" rel="stylesheet" />
                                    <script src="/boletimCarga-portlet/Content/js/bootstrap3.3.7/bootstrap.min.js"></script>

                                    <!-- Basic Styles -->
                                    <link rel="stylesheet" media="screen" href="/boletimCarga-portlet/Content/css/bootstrap/bootstrap.min.css" />
                                    <link rel="stylesheet" media="screen" href="/boletimCarga-portlet/Content/css/bootstrap/bootstrap-responsive.min.css" />
                                    <link rel="stylesheet" media="screen" href="/boletimCarga-portlet/Content/css/datatables/jquery.dataTables.min.css" />
                                    <link rel="stylesheet" media="screen" href="/boletimCarga-portlet/Content/css/datatables/dataTables.bootstrap.css" />
                                    <link rel="stylesheet" media="screen" href="/boletimCarga-portlet/Content/css/libra/index.css" />
                                    <link rel="stylesheet" media="screen" href="/boletimCarga-portlet/Content/css/libra/boletim.css" />

                                    <!-- ANGULAR JS -->
                                    <script src="/boletimCarga-portlet/Content/js/angularjs1.6.6/angular.min.js"></script>

                                    <!-- DATATABLES JS -->
                                    <script src="/boletimCarga-portlet/Content/js/datatables/jquery.dataTables.min.js"></script>
                                    <script src="/boletimCarga-portlet/Content/js/datatables/jquery.dataTables.bootstrap.js"></script>
                                    <script src="/boletimCarga-portlet/Content/js/datatables/i18n/Portuguese-Brasil.js"></script>
                                    <script src="/boletimCarga-portlet/js/libs/ui-bootstrap-tpls-0.9.0.js"></script>
                                    <script src="/boletimCarga-portlet/js/libs/jquery/jquery.maskedinput.js"></script>
                                    <script src="/boletimCarga-portlet/js/libs/jquery/jquery-ui-1.10.4.custom.js"></script>
                                    <script src="/boletimCarga-portlet/js/libs/jquery/jquery-ui-sliderAccess.js"></script>
                                    <script src="/boletimCarga-portlet/js/site/master.framework.js"></script>

                                    <!-- Mask -->
                                    <script src="/boletimCarga-portlet/Content/js/jquery.inputmask/jquery.mask.js"></script>

                                    <!-- Angular JS APP -->
                                    <script src="/boletimCarga-portlet/js/modules/boletimapp.js"></script>

                                    <!-- Controller -->
                                    <script src="/boletimCarga-portlet/js/controller/boletimcontroller.js"></script>

                                    <!-- Service -->
                                    <script src="/boletimCarga-portlet/js/service/boletimservice.js"></script>

                                    <!-- Directive -->
                                    <script src="/boletimCarga-portlet/js/directives/formatCurrency.js"></script>

                                    <input type="hidden" id="linkMontado" value="${pageContext.request.serverName}" />

                                    <script>
                                        $(document).ready(function() {
                                            // if (window.location.href.split("8443").length > 1) {
                                            //     window.location = window.location.href.replace("8443", "8543");
                                            // } else if (window.location.href.split("8543").length == 1) {
                                            //     var location = window.location.href.replace($('#linkMontado').val(), $('#linkMontado').val() + ":8543");
                                            //     location = location.replace("http", "https");
                                            //     window.location = location;
                                            // }
                                        });
                                    </script>

                                    <script>
                                        function reSend(unitNbr) {
                                            angular.element(document.getElementById('principal')).scope().reSend(
                                                unitNbr);
                                        }
                                    </script>

                                    <div class="container-fluid">

                                        <h2 class="page-header">Boletim de Carga</h2>

                                        <div class="modal-backdrop" id="showLoadModal" style="display: none;">
                                            <div class="fade in" style="opacity: 1; position: absolute; top: 50%; left: 50%; width: 500px; height: 200px; margin-left: -250px; margin-top: -100px; text-align: center;">
                                                <label class="" style="text-align: center !important;"> <img
			src="/boletimCarga-portlet/Content/img/ajax-loader.gif">
		</label>
                                            </div>
                                        </div>
                                        <div ng-app="BoletimApp">
                                            <div ng-controller="BoletimCargaController as BoletimCarga" id="principal">

                                                <div class="modal-backdrop" id="showDialog" ng-show="BoletimCarga.showDialog" style="display: none;" ng-cloack>
                                                    <div class="alert alert-block alert-error fade in" style="opacity: 1; position: absolute; top: 50%; left: 50%; width: 500px; height: 200px; margin-left: -250px; margin-top: -100px; text-align: center; overflow: hidden;">
                                                        <h4 class="alert-heading">Atenção</h4>
                                                        <br />
                                                        <p class="paragrafoAlert" ng-bind-html="BoletimCarga.to_trusted(errorMessage)"></p>
                                                        <p class="paragrafoButton">
                                                            <a class="" title="Ok" href="#" ng-click="BoletimCarga.closeDialog()"> <i class="icon-ok icon-white circle-medium danger"></i>
                                                            </a>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="modal-backdrop" id="showDialogSuccess" ng-show="BoletimCarga.showDialogSuccess" style="display: none;" ng-cloack>
                                                    <div class="alert-block alert-success fade in" style="opacity: 1; position: absolute; top: 50%; left: 50%; width: 500px; height: 200px; margin-left: -250px; margin-top: -100px; text-align: center; overflow: hidden;">
                                                        <h4 class="alert-heading">Atenção</h4>
                                                        <br />
                                                        <p class="paragrafoAlert" ng-bind-html="BoletimCarga.to_trusted(message)"></p>
                                                        <p class="paragrafoButton">
                                                            <a class="" title="Ok" href="#" ng-click="BoletimCarga.closeDialogSuccess()">
                                                                <i class="icon-ok icon-white circle-medium success"></i>
                                                            </a>
                                                        </p>
                                                    </div>
                                                </div>

                                                <!-- RDF - Busca por VesselVisit -->
                                                <div class="row-fluid add-mb">
                                                    <div class="span5">
                                                        <input class="input-block-level" type="text" placeholder="Procurar Vessel Visit" ng-model="vessel" typeahead="vessel as vessel.id for vessel in vessels | filter:$viewValue" typeahead-min-length='1' typeahead-template-url="/BoletimCargaPortlet/paginas/customTemplate.html">
                                                    </div>
                                                    <div class="span2">
                                                        <a class="" href="#" ng-click="findVesselVisitInfo()" title="Pesquisar"> <i class="icon-search icon-white circle-medium"></i>
                                                        </a>
                                                        <a class="" href="#" ng-click="limparInfo()" title="Limpar"> <i class="icon-eraser icon-white circle-medium"></i>
                                                        </a>
                                                    </div>
                                                    <label class="pull-right"> E-CPF: {{hasEcpf}} </label>
                                                </div>

                                                <div class="row-fluid add-mb">
                                                    <div class="span2">
                                                        <select class="input-block-level" ng-model="field" ng-options="field.desc for field in fields" ng-click="changeCombo(field)"></select>
                                                    </div>
                                                    <div class="span3" ng-show="!showCombo">
                                                        <input class="input-block-level" ng-model="fieldCont" ng-keyup="toUpperCase()" />
                                                    </div>
                                                    <div class="span3" ng-show="showCombo">
                                                        <select class="input-block-level" ng-model="fieldCont" ng-options="option.descricao for option in options"></select>
                                                    </div>
                                                    <label class="pull-right"> N�mero do CPF: {{ecpfNbr}} </label>
                                                </div>

                                                <!-- RDF - Boletim Pendentes-->
                                                <div class="row-fluid">
                                                    <table class="table table-bordered" id="boletinsPendentes">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>Pacote</th>
                                                                <th>Qtd. Itens</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr ng-repeat="boletim in boletins" class="packages">

                                                                <td align="center" class="details-control" ng-click="addChildRow(boletim)" ng-show="boletim.boletimId == 'PENDENCIAS'">
                                                                    <i class="icon-plus icon-black iconparent"></i>
                                                                </td>
                                                                <td align="center" ng-bind="boletim.boletimId" ng-show="boletim.boletimId == 'PENDENCIAS'"></td>
                                                                <td align="center" ng-bind="boletim.qtdeItens" ng-show="boletim.boletimId == 'PENDENCIAS'"></td>

                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <!-- RDF - Pacotes -->
                                                <!--  pacote normal -->

                                                <div class="row-fluid add-mb">
                                                    <div class="span12">
                                                        Procurar por:
                                                    </div>
                                                    <div class="span2">
                                                        <select class="input-block-level" ng-model="BoletimCarga.SelectedSearchOption" ng-change="BoletimCarga.ShowField()" ng-options="SearchOption.Description for SearchOption in BoletimCarga.SearchOptions"></select>
                                                    </div>
                                                    <div class="span2" ng-show="BoletimCarga.VesselVisitFlag">
                                                        <input ng-model="BoletimCarga.fieldOptions" class="input-block-level" type="text" placeholder="Vessel Visit">
                                                    </div>
                                                    <div class="span2" ng-show="BoletimCarga.GKeyFlag">
                                                        <input ng-model="BoletimCarga.fieldOptions" class="input-block-level" type="text" placeholder="GKey" format="number">
                                                    </div>
                                                    <div class="span2" ng-show="BoletimCarga.DataFlag">
                                                        <input ng-model="BoletimCarga.fieldOptions" data-mask="00/00/0000" class="input-block-level" type="text" placeholder="Data">
                                                    </div>
                                                    <div class="span3" ng-show="BoletimCarga.StatusFlag">
                                                        <input ng-model="BoletimCarga.fieldOptions" class="input-block-level" type="text" placeholder="Status">

                                                    </div>
                                                    <div class="span2">
                                                        <a class="" href="#" ng-click="BoletimCarga.findVesselVisitInfo()" title="Pesquisar"> <i class="icon-search icon-white circle-medium"></i>
                                                        </a>
                                                        <a class="" href="#" ng-click="BoletimCarga.limparInfo()" title="Limpar"> <i class="icon-eraser icon-white circle-medium"></i>
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="row-fluid">
                                                    <table class="table table-bordered" id="boletins">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>Vessel Visit</th>
                                                                <th>GKey</th>
                                                                <th>Data</th>
                                                                <th>Tipo</th>
                                                                <th>Status</th>
                                                                <th>Codigo</th>
                                                                <th>Descrição</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr ng-repeat="boletim in BoletimCarga.boletins" class="packages">
                                                                <td align="center" class=" details-control" ng-click="BoletimCarga.addChildRow(boletim)" ng-show="BoletimCarga.boletim.boletimId != 'PENDENCIAS'">
                                                                    <i class="icon-plus icon-black iconparent" ng-click="BoletimCarga.addChildRow(boletim)" ng-show="BoletimCarga.boletim.boletimId != 'PENDENCIAS'"></i>
                                                                </td>

                                                                <td align="center" ng-bind="boletim.boletimId" ng-show="boletim.boletimId != 'PENDENCIAS'"></td>
                                                                <td align="center" ng-bind="boletim.dataRetornoFormatada" ng-show="boletim.boletimId != 'PENDENCIAS'"></td>
                                                                <td align="center" ng-bind="boletim.operacao" ng-show="boletim.boletimId != 'PENDENCIAS'"></td>
                                                                <td align="center" ng-bind="boletim.restow" ng-show="boletim.boletimId != 'PENDENCIAS'"></td>
                                                                <td align="center" ng-bind="boletim.status" ng-show="boletim.boletimId != 'PENDENCIAS'"></td>
                                                                <td align="center" ng-bind="boletim.status" ng-show="boletim.boletimId != 'PENDENCIAS'"></td>
                                                                <td align="center" ng-bind="boletim.status" ng-show="boletim.boletimId != 'PENDENCIAS'"></td>

                                                                <td align="center" ng-click="BoletimCarga.addChildRow(boletim)" ng-show="boletim.boletimId != 'PENDENCIAS'">
                                                                    <div ng-show="boletim.status == ' ' && boletim.qtdeItens < 50 && boletim.boletimId != 'PENDENCIAS'">
                                                                        <a class="btn btn-primary" ng-click="BoletimCarga.reSend(boletim)" ng-show="boletim.status == ' ' && boletim.qtdeItens < 50 && boletim.boletimId != 'PENDENCIAS'"> <i class="icon-ok icon-white"></i>&nbsp; re-Envio
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                    </div>



                                    <%
//} else {
%>
                                        <!-- REMOVIDO <div class="alert alert-error">Voc� n�o tem permiss�o de acessar essa funcionalidade</div> -->
                                        <%
//}
%>